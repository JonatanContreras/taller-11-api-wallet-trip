import mysql from "mysql";

export default function get_sitio() {
  //1. definir los parámetros de conexión a mysql


  let parametros = {
    host: "localhost",
    user: "root",
    password: "contraseña",
    database: "wallettrip_db",
  };
  
  let conexion = mysql.createConnection(parametros);

  conexion.connect((err)=>{

      if(err){
          console.log("Error en la conexión a la base de datos!!");
          console.log(err.message);
          return false;
      }else{
          console.log("Conectado al servidor MySQL");
          return true;
      }
  });
  
  //2. Realizar consulta SQL
  
  let consulta = `SELECT idplace, name, description FROM place WHERE idplace=? OR idplace=?`;

  conexion.query(consulta,[peticion.query.a, peticion.query.b] ,(err, resultado, fields) => {
    if (err) {
      console.error("error" + err.message);
    } else {
      console.log(resultado);
    }
  });
  //3. Desconectarnos al servidor mysql

  conexion.end();

  return true;
}
