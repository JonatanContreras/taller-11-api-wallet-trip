import mysql from 'mysql';

export default function set_place(){

    //1. Conectarse a la base de datos

    let parametros={

        host:"localhost",
        user:"root",
        password:"contraseña",
        database:"wallettrip_db"
    };

    let conexion= mysql.createConnection(parametros);

    conexion.connect((err)=>{

        if(err){
            console.log("Error en la conexión a la base de datos!!");
            console.log(err.message);
            return false;
        }else{
            console.log("Conectado al servidor MySQL");
            return true;
        }
    });

    //2. Realizar la insercion de datos

    let consulta=`INSERT INTO place (idplace, name, description, country_code, region, location) 
    VALUES (5,
        'Panadería La última mogolla', 
        'Lo último en delicias',
        'CO', 
        'Bogotá, D.C',
        ST_GeomFromText('POINT(4.611050940917937 -74.07046796181972)')
        ) `;

    conexion.query(consulta);

    //3. Cerrar la conexión

    conexion.end();

    return "Insertado!!";

}
